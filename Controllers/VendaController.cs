using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PaymentContext _context;
        public VendaController(PaymentContext context)
        {
            _context = context;
        }

        //Registro de Vendas
        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda){
            try{
                var novaVenda = new Venda();
                novaVenda.Descricao = venda.Descricao;
                novaVenda.DataVenda = venda.DataVenda;
                novaVenda.VendedorId = venda.VendedorId;
                novaVenda.Status = 0;
                if(_context.Vendedor.Find(venda.VendedorId) == null){
                    return Conflict("Vendedor não Encontrado.");
                }

                _context.Add(novaVenda);
                _context.SaveChanges();
                venda.Id = novaVenda.Id;

                foreach(var item in venda.ItensVenda){
                    Venda novoItem = new Venda();
                    novoItem.Id = novaVenda.Id;
                    novoItem.Descricao = item.Descricao;
                    _context.Add(novoItem);
                    _context.SaveChanges();  
                    item.Id = novoItem.Id;              
                }            
                
                return CreatedAtAction(nameof(ObterVendasPorId), new { id = novaVenda.Id }, venda);
            }catch(Exception erro){
                 return BadRequest(new { Erro = "Ocorreu um erro durante a execução.", Mensagem = erro.Message});
            }
        }

        //Obter venda por Id
        [HttpGet("{id}")]
        public IActionResult ObterVendasPorId(int id){
            try{
                var resultado = new Venda();
                var venda = _context.Venda.Find(id);
                
                if(venda == null){
                    return NotFound();
                }

                resultado.Id = id;
                resultado.Descricao = venda.Descricao;
                resultado.DataVenda = venda.DataVenda;
                resultado.Status = venda.Status;
                resultado.VendedorId = venda.VendedorId;

                var itensVenda = _context.Produto.Where(x => x.VendaId == id).ToList();

                foreach(var item in itensVenda){
                    var itemVenda = new Produto();
                    itemVenda.VendaId = id;
                    itemVenda.Id = item.Id;
                    itemVenda.Descricao = item.Descricao;

                    _context.Add(itemVenda);
                }
                return Ok(resultado);
            }catch(Exception erro){
                 return BadRequest(new { Erro = "Ocorreu um erro durante a execução.",Mensagem = erro.Message});
            }
        }

        //Atualizar Status da venda
        [Route("AtualizarStatusVenda")]
        [HttpPut]
        public IActionResult AtualizarStatusVenda(int id, EnumStatusVenda statusVenda){
            try{
                var venda = _context.Venda.Find(id);
                //Tratamento do Status "Aguardando Pagamento"
                if(venda.Status == EnumStatusVenda.AguardandoPagamento){
                    if(statusVenda != EnumStatusVenda.PagamentoAprovado && statusVenda != EnumStatusVenda.Cancelada){
                        return BadRequest(new { Erro = "Não é possível alterar a Venda para este status."});
                    }
                }
                //Tratamento do Status "Pagamento Aprovado"
                else if(venda.Status == EnumStatusVenda.PagamentoAprovado){
                    if(statusVenda != EnumStatusVenda.EnviadoTransportadora && statusVenda != EnumStatusVenda.Cancelada){
                        return BadRequest(new { Erro = "Não é possível alterar a Venda para este status."});
                    }
                }
                //Tratamento do Status "Enviado para Transportador"
                else if(venda.Status == EnumStatusVenda.EnviadoTransportadora){
                    if(statusVenda != EnumStatusVenda.Entregue){
                        return BadRequest(new { Erro = "Não é possível alterar a Venda para este status."});
                    }
                }else{
                    return BadRequest(new { Erro ="Não é possível alterar a Venda para este status."});
                }

                venda.Status = statusVenda;
                _context.Venda.Update(venda);
                _context.SaveChanges();
                return Ok("Status alterado com sucesso!");
            }catch(Exception erro){
                 return BadRequest(new { Erro = "Ocorreu um erro durante a execução.", Mensagem = erro.Message});
            }
        }

    }
}
