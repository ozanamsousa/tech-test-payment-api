namespace tech_test_payment_api.Models
{
    public class Produto 
    {
         public int Id { get; set; }
        public string Descricao { get; set; }
        public string Unidade { get; set; }
        public decimal Quantidade { get; set; }
        public int VendaId { get; set; }
        public Venda Venda { get; set; }

    }
    
}