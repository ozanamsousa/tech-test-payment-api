using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public EnumStatusVenda Status { get; set; }
        public DateTime? DataVenda { get; set; }
        
        public int VendedorId { get; set; }
        public List<Venda> ItensVenda { get; set; }


}
}