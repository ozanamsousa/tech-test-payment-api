using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class PaymentContext : DbContext
    {
    
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options)
        {
            
        }

        public DbSet<Venda> Venda { get; set; }
        public DbSet<Vendedor> Vendedor {get; set;}
        public DbSet<Produto> Produto { get; set; }
    }
}